<?php
/**
 * @file
 * Commerce payment template (checkout pane).
 *
 * @ingroup commerce_pay_with_a_tweet
 */
?>

<p><strong><?php print $title; ?></strong></p>
<p>
  <?php print $img; ?>
  <?php print $tweet; ?>
</p>
